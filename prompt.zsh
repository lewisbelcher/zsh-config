setopt prompt_subst
export ZLE_RPROMPT_INDENT=0

_prompt_start=""
_prompt_sep=" %f/ "
_prompt_end=" %f> "
_prompt_base="$_prompt_start%F{cyan}%n@%m"
_prompt_git_config=(
  "Your branch is ahead; ;"
  "Your branch is behind; ;"
  "Changes to be committed; ;yellow"
  "Changes not staged for commit; ;yellow"
  "Untracked files; ;yellow"
  "Your stash currently has; ;"
)

_prompt_dir() {
  local p="‥/"
  [ "`dirname $PWD`" = / ] && p=" "
  echo -n "$_prompt_sep%F{123} $p%1d"
}

_prompt_venv() {
  [[ -n $VIRTUAL_ENV ]] && echo -n "$_prompt_sep%F{120} $(basename $(dirname $VIRTUAL_ENV))"
}

_prompt_njobs() {
  local n=$(jobs | grep -v 'pwd now' | wc -l)
  [[ n -gt 0 ]] && echo -n "$_prompt_sep%F{13}$n"
}

_prompt_git() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == "true" ]]; then
    # See http://zsh.sourceforge.net/Doc/Release/Expansion.html for advanced
    # shell expansion
    local state=$(git status --show-stash)
    local str=" ${${(f)state}[1]##* }"      # Last word on first line
    local colour=green                       # Default colour
    for msg in $_prompt_git_config; do
      xmsg=(${(s.;.)msg})                    # `msg` split by `;`
      if [[ $state =~ ${xmsg[1]} ]]; then    # If `git status` contains `xmsg[1]`
        str+=${xmsg[2]}                      # Add symbol
        colour=${xmsg[3]:-${colour}}         # Change colour (if applicable)
      fi
    done
    echo -n "${_prompt_sep}%F{$colour}${str}"
  fi
}

preexec() {
  _prompt_timer=$SECONDS # Number of seconds since shell started
}

precmd() {
  _prompt_status=$status
  if [ $_prompt_timer ]; then
    _prompt_cmd_time=$(($SECONDS - $_prompt_timer))
  else
    unset _prompt_cmd_time
  fi
  unset _prompt_timer # Unset so the timer only shows when a command was run
}

_prompt_time() {
  if [ $_prompt_cmd_time ] && [[ $_prompt_cmd_time -gt 2 ]]; then
    echo -n "%F{240}${_prompt_cmd_time}s"
  fi
}

_prompt_status() {
  if [ $_prompt_cmd_time ] && [[ $_prompt_status -ne "0" ]]; then
    echo -n " %F{red}($_prompt_status)"
  fi
}

export PROMPT="$_prompt_base\$(_prompt_dir)\$(_prompt_venv)\$(_prompt_njobs)\$(_prompt_git)$_prompt_end"
export RPROMPT="\$(_prompt_time)\$(_prompt_status)"

# FZF Options

if [ -x $(command -v fd) ] && [ -x $(command -v fzf) ] ; then
  export FZF_DEFAULT_COMMAND='fd --type f --no-ignore --hidden'

  # Use fd for listing path candidates.
  _fzf_compgen_path() {
    fd --hidden --follow --exclude ".git" . "$1"
  }

  # Use fd to generate the list for directory completion
  _fzf_compgen_dir() {
    fd --type d --hidden --follow --exclude ".git" . "$1"
  }
fi

my-fzf-cd-widget() {
  local selected=$(echo $dirstack | tr ' ' '\n' | sort | uniq | fzf --height 8% --no-sort --no-multi --exact)
  local ret=$?
  if [ -n "$selected" ]; then
    LBUFFER="cd $selected"
  fi
  zle reset-prompt
  return $ret
}
zle -N my-fzf-cd-widget

my-replace-widget() {
  local pattern=$(echo $LBUFFER | grep -Po 'replace \K.+')
  LBUFFER="rg -l '$pattern' | xargs sed -i 's/$pattern//g'"
  zle reset-prompt
}
zle -N my-replace-widget

# Load key-bindings TODO: Make OS agnostic
if [ -f "/usr/share/fzf/key-bindings.zsh" ] ; then
  source /usr/share/fzf/key-bindings.zsh

  if [ -f "/usr/share/fzf/completion.zsh" ] ; then
    source /usr/share/fzf/completion.zsh
  fi

  # Intercept tab to do cool stuff:
  _tab_intercept() {
    if [[ $#BUFFER == 0 ]]; then
      zle fzf-history-widget
    elif [[ $BUFFER == "cd" ]]; then
      zle my-fzf-cd-widget
    elif [[ $BUFFER =~ "^replace" ]]; then
      zle my-replace-widget
    else
      zle expand-or-complete
    fi
  }
  zle -N _tab_intercept
  bindkey '^I' _tab_intercept
fi

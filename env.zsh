# Env variables
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export KEYTIMEOUT=1 # Reduce delay for listening to escape keys (faster NORMAL)
export EDITOR=vim
export MOZ_ENABLE_WAYLAND=1
export VIRTUAL_ENV_DISABLE_PROMPT=1
export PATH=$PATH:$HOME/bin
export WASMTIME_HOME="$HOME/.wasmtime"
export PATH="$WASMTIME_HOME/bin:$PATH"

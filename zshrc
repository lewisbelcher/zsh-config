# History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=50000
SAVEHIST=50000
setopt appendhistory inc_append_history share_history auto_pushd
unsetopt autocd extendedglob
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' menu select
autoload -Uz compinit && compinit

# Autoload venv when entering a directory that contains one
autoload -U add-zsh-hook
add-zsh-hook -Uz chpwd () { [ -d .venv ] && source .venv/bin/activate }

# Make navigation stop an non-alphanumeric characters
export WORDCHARS=''

# Command completion with up/down arrows
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
autoload -U edit-command-line
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
zle -N edit-command-line

eval `dircolors -b`  # set dircolors

# Set keybindings
bindkey -v  # Using vi mode
for mode in "" "-M vicmd" ; do
  eval "bindkey $mode '^[[H' beginning-of-line"
  eval "bindkey $mode '^[[F' end-of-line"
  eval "bindkey $mode '^[[1;5C' forward-word"
  eval "bindkey $mode '^[[1;5D' backward-word"
  eval "bindkey $mode '^H' backward-delete-word"  # ctrl-backspace
  eval "bindkey $mode '^W' backward-delete-word"
  eval "bindkey $mode '^U' backward-kill-line"
  eval "bindkey $mode '^K' kill-line"
  eval "bindkey $mode '^Y' yank"
  eval "bindkey $mode '^[[3;5~' delete-word"
  eval "bindkey $mode '^[[3~' delete-char"
  eval "bindkey $mode '^?' backward-delete-char"
  eval "bindkey $mode '^V' edit-command-line"
  eval "bindkey $mode '^[[A' up-line-or-beginning-search"
  eval "bindkey $mode '^[[B' down-line-or-beginning-search"
done

# Source local files
_path=$(dirname $(realpath ${(%):-%x}))
source $_path/alias.zsh
source $_path/env.zsh
source $_path/fzf.zsh
source $_path/prompt.zsh
unset _path
